print('----------------Meniu----------------')
print('1."Bors" - 40 USD')
print('2.Placinta - 10 USD')
print('3.Compot- 15 USD')
print('---------------------------------------')
print()
print()


#DATA - 
food_1_name = 'Bors'
food_1_price = 40.00

food_2_name = 'Placinta'
food_2_price = 10.00

drink_1_name = 'Compot'
drink_1_price = 15.00

#DATA - input
food_1_q = int(input(f'{f"How much {food_1_name} want?":25}' ))
food_2_q = int(input(f'How much {food_2_name} want?      ' ))
drink_1_q = int(input(f'How much {drink_1_name} want?  ' ))
print()

# COST
food_1_cost = food_1_price * food_1_q
print (f'Trebuie sa achitati {food_1_cost:5} pentru {food_1_name}')
food_2_cost = food_2_price * food_2_q
print (f'Trebuie sa achitati {food_2_cost:5} pentru {food_2_name}')

drink_1_cost = drink_1_price * drink_1_q
print (f'Trebuie sa achitati {drink_1_cost:5} pentru {drink_1_name}')
print()
total_cost = food_1_cost + food_2_cost + drink_1_cost
print (f'Trebuie sa achitati per total {total_cost}  USD ')